package node

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"

	"gitee.com/cmlfxz/gin-k8s/src/handle/metric"
	"gitee.com/cmlfxz/gin-k8s/src/handle/pod"
	"gitee.com/cmlfxz/gin-k8s/src/lib/gredis"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"gitee.com/cmlfxz/gin-k8s/src/util"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	Redis_Key_TimeOut int = 15 * 60
)

type DiskStat struct {
	Total float64 `json:"total"`
}
type PodStat struct {
	Total float64 `json:"total"`
	Usage float64 `json:"usage"`
}
type MemoryStat struct {
	Total float64 `json:"total"`
	Usage float64 `json:"usage"`
}
type CpuStat struct {
	Total float64 `json:"total"`
	Usage float64 `json:"usage"`
}
type NodeStat struct {
	Cpu    CpuStat    `json:"cpu"`
	Memory MemoryStat `json:"memory"`
	Pod    PodStat    `json:"pod"`
	Disk   DiskStat   `json:"disk"`
}

func node_stat(node *v1.Node) (map[string]interface{}, error) {
	meta := node.ObjectMeta
	name := meta.Name
	role := meta.Labels["kubernetes.io/role"]
	capacity := node.Status.Capacity
	usage, err := metric.Node_Usage(name)
	if err != nil {
		return nil, errors.New("get node usage fail")
	}
	cpu_total := float64(capacity.Cpu().MilliValue())
	cpu_usage := usage.Cpu

	memory_total := util.Handle_Memory(capacity.Memory().Value())
	memory_usage := usage.Memory

	disk_space := util.Handle_Disk(capacity.StorageEphemeral().Value())

	pod_total := capacity.Pods().Value()

	// 统计node下所有的pod的资源情况
	node_resources, _ := pod.Node_Resources_Stat(name)

	// 获取的是request和 limits的总体资源
	resources := node_resources.Resources
	pod_num := node_resources.Pod_Num

	stat := NodeStat{
		Cpu: CpuStat{
			Total: cpu_total,
			Usage: cpu_usage,
		},
		Memory: MemoryStat{
			Total: memory_total,
			Usage: memory_usage,
		},
		Pod: PodStat{
			Total: float64(pod_total),
			Usage: float64(pod_num),
		},
		Disk: DiskStat{
			Total: disk_space,
		},
	}
	detail := map[string]interface{}{
		"name":           name,
		"role":           role,
		"node_stat":      stat,
		"node_resources": resources,
	}
	return detail, nil
}

func Get_Node_Stat(c *gin.Context) {

	cluster_name := c.Request.Header.Get("cluster_name")
	if cluster_name == "" {
		c.JSON(500, gin.H{"fail": "header无法获取集群名称"})
		return
	}
	key := fmt.Sprintf("%s_node_stat_list", cluster_name)

	redisClient := gredis.NewRedisClient(0)
	node_stat_list, _ := redisClient.Get(key).Result()
	list := make([]map[string]interface{}, 0)
	if node_stat_list == "" {
		clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
		if err != nil {
			c.JSON(500, gin.H{"fail": "k8s连接不上"})
			return
		}
		client := clientset.CoreV1()
		var nodes *v1.NodeList
		nodes, _ = client.Nodes().List(context.TODO(), metaV1.ListOptions{})
		for _, node := range nodes.Items {
			stat, _ := node_stat(&node)
			list = append(list, stat)
		}

		jsonStr, _ := json.Marshal(list)
		value := string(jsonStr)
		redisClient.Set(key, value, time.Minute*15)
		node_stat_list = value
	}
	c.String(200, node_stat_list)

}
