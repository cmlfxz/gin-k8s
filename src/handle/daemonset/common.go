package daemonset

type InputData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
}
