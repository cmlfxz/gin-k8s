package deployment

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_deployment_list     = "http://192.168.11.5:8090/k8s/get_deployment_list"
	get_problem_deployments = "http://192.168.11.5:8090/k8s/get_problem_deployments"
	get_deployment_detail   = "http://192.168.11.5:8090/k8s/get_deployment_detail"
)

func Test_Deployment(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "all"
	result, err := httpclient.Post(get_deployment_list, data, headers)
	fmt.Println(string(result), err)
}
func Test_Problems(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "all"
	result, err := httpclient.Post(get_problem_deployments, data, headers)
	fmt.Println(string(result), err)
}

func Test_Detail(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["name"] = "flask-admin"
	data["namespace"] = "ms-test"
	result, err := httpclient.Post(get_deployment_detail, data, headers)
	fmt.Println(string(result), err)
}
