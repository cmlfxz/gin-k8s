package pod

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
)

func pod_log(namespace string, name string, container string) (string, error) {

	// var kubeconfig *string
	// curPath, _ := os.Getwd()
	// kubeconfig = flag.String("k8s", filepath.Join(curPath, "k8s"), "(optional) relative path to the kubeconfig file")
	// flag.Parse()
	// config, _ := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	// clientset, err := kubernetes.NewForConfig(config)
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		return "", errors.New("k8s连接不上")
	}
	client := clientset.CoreV1()
	var req *restclient.Request
	// var lines int64 = 5

	// req = client.Pods(namespace).GetLogs(name, &v1.PodLogOptions{Container: container, TailLines: &lines, Follow: true})

	// req = client.Pods(namespace).GetLogs(name, &v1.PodLogOptions{Container: container, Follow: true})

	req = client.Pods(namespace).GetLogs(name, &v1.PodLogOptions{Container: container})
	podLogs, err := req.Stream(context.TODO())
	if err != nil {
		return "", errors.New("error in opening stream")
	}
	defer podLogs.Close()

	buf := new(bytes.Buffer)
	_, err = io.Copy(buf, podLogs)
	if err != nil {
		return "", errors.New("error in copy information from podLogs to buf")
	}
	str := buf.String()
	// fmt.Println(str)
	return str, nil

	// buf := bufio.NewReaderSize(podLogs, 64*1024)
	// for {
	// 	bytes, err := buf.ReadBytes('\n')
	// 	fmt.Print(string(bytes))
	// 	if err != nil {
	// 		if err != io.EOF {
	// 			return "", err
	// 		}
	// 		return "", nil
	// 	}
	// }
}

type LogData struct {
	Namespace string `json:"namespace"`
	Name      string `json:"name"`
	Container string `json:"container"`
}

func Get_Pod_Log(c *gin.Context) {
	var data LogData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	log, err := pod_log(data.Namespace, data.Name, data.Container)
	fmt.Printf("eeeee:%v\n", err)
	if err != nil {
		c.JSON(500, gin.H{"fail": "get log fail", "error": err})
		return
	}
	c.String(200, log)
}
