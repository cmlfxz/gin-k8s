package service

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)



func Get_Service_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var services *v1.ServiceList
	if namespace == "all" || namespace == "" {
		services, _ = client.Services("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		services, _ = client.Services(namespace).List(context.TODO(), metaV1.ListOptions{})
	}

	list := make([]map[string]interface{}, 0)
	for _, service := range services.Items {
		meta := service.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		labels := meta.Labels
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		spec := service.Spec
		cluster_ip := spec.ClusterIP
		ports := spec.Ports
		selector := spec.Selector
		service_type := spec.Type
		internal_endpoints := make([]string, 0)
		for _, p := range ports {
			endpoint := fmt.Sprintf("%s.%s:%d %s", name, namespace, p.Port, p.Protocol)
			internal_endpoints = append(internal_endpoints, endpoint)
			if p.NodePort > 0 {
				endpoint2 := fmt.Sprintf("%s.%s:%d %s", name, namespace, p.NodePort, p.Protocol)
				internal_endpoints = append(internal_endpoints, endpoint2)
			}
		}
		detail := map[string]interface{}{
			"name":               name,
			"namespace":          namespace,
			"service_type":       service_type,
			"ports":              ports,
			"internal_endpoints": internal_endpoints,
			"labels":             labels,
			"cluster_ip":         cluster_ip,
			"selector":           selector,
			"create_time":        create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Delete_Service(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参有误"})
	} else {
		namespace := data.Namespace
		name := data.Name
		clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
		if err != nil {
			c.JSON(500, gin.H{"fail": "k8s连接不上"})
		} else {
			client := clientset.CoreV1()
			if namespace == "all" || namespace == "" {
				c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
			} else {
				err := client.Services(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
				if err == nil {
					c.JSON(200, gin.H{"ok": "删除成功"})
				} else {
					c.JSON(500, gin.H{"fail": "删除失败"})
				}
			}
		}
	}
}
