package auth

import (
	"fmt"
	"testing"

	"gitee.com/cmlfxz/gin-k8s/src/lib/httpclient"
)

var (
	get_service_account_list = "http://192.168.11.5:8090/k8s/get_service_account_list"
	delete_service_account   = "http://192.168.11.5:8090/k8s/delete_service_account"
)

func Test_Auth(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := make(map[string]interface{})
	data["namespace"] = "all"
	result, err := httpclient.Post(get_service_account_list, data, headers)
	fmt.Println(string(result), err)
}
func Test_Delete(t *testing.T) {
	headers := make(map[string]string)
	headers["cluster_name"] = "k3s_112"
	data := map[string]interface{}{
		"name":      "test-sa",
		"namespace": "ms-test",
	}
	result, err := httpclient.Post(delete_service_account, data, headers)
	fmt.Println(string(result), err)
}
