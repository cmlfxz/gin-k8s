package namespace

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func namespace_detail(namespace *v1.Namespace) map[string]interface{} {
	meta := namespace.ObjectMeta
	name := meta.Name
	labels := meta.Labels
	create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	status := namespace.Status.Phase
	detail := map[string]interface{}{
		"name":        name,
		"status":      status,
		"labels":      labels,
		"create_time": create_time,
	}
	return detail
}

func Get_Namespace_List(c *gin.Context) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var namespaces *v1.NamespaceList

	namespaces, err = client.Namespaces().List(context.TODO(), metaV1.ListOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "获取namespace资源失败"})
		return
	}
	list := make([]map[string]interface{}, 0)
	for _, namespace := range namespaces.Items {
		detail := namespace_detail(&namespace)
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Get_Namespace_Name_List(c *gin.Context) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var namespaces *v1.NamespaceList

	namespaces, _ = client.Namespaces().List(context.TODO(), metaV1.ListOptions{})
	list := make([]string, 0)
	for _, namespace := range namespaces.Items {
		name := namespace.ObjectMeta.Name
		list = append(list, name)
	}
	fmt.Println(list)
	c.JSON(200, list)
}

type DeleteData struct {
	Name string `json:"name"`
}

func Delete_Namespace(c *gin.Context) {
	var data DeleteData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	err = client.Namespaces().Delete(context.TODO(), name, metaV1.DeleteOptions{})
	if err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败"})
	}
}

type CreateData struct {
	Project      string `json:"project_name"`
	Env          string `json:"env_name"`
	Istio_Inject string `json:"istio_inject"`
}

func Create_Namespace(c *gin.Context) {
	var data CreateData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := fmt.Sprintf("%s-%s", data.Project, data.Env)
	labels := make(map[string]string)
	if data.Istio_Inject == "on" {
		labels["istio-injection"] = "enabled"
	}
	ok, err := Create(name, labels)
	if ok {
		c.JSON(200, gin.H{"ok": "创建成功"})
	} else {
		c.JSON(500, gin.H{"ok": "创建失败", "error": err})
	}
}

type UpdateData struct {
	Name   string            `json:"name"`
	Labels map[string]string `json:"labels"`
	Action string            `json:"action"`
}

func Update_Namespace(c *gin.Context) {
	var data UpdateData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(200, gin.H{"ok": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	namespace, err := client.Namespaces().Get(context.TODO(), data.Name, metaV1.GetOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "找不到资源"})
		return
	}

	labels := data.Labels
	if data.Action == "remove_istio_inject" {
		delete(labels, "istio-injection")
	} else if data.Action == "add_istio_inject" {
		labels["istio-injection"] = "enabled"
	} else {
		c.JSON(500, gin.H{"fail": "不支持的action"})
		return
	}
	namespace.ObjectMeta.Labels = labels

	_, err = client.Namespaces().Update(context.TODO(), namespace, metaV1.UpdateOptions{})
	if err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败", "error": err})
	}
}
