package namespace

import (
	"context"
	"errors"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func Create(name string, labels map[string]string) (bool, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		return false, errors.New("k8s链接失败")
	}
	client := clientset.CoreV1()
	var metadata metaV1.ObjectMeta
	if len(labels) != 0 {
		metadata = metaV1.ObjectMeta{Name: name, Labels: labels}
	} else {
		metadata = metaV1.ObjectMeta{Name: name}
	}
	fmt.Println(metadata)
	namespace := v1.Namespace{
		ObjectMeta: metadata,
		TypeMeta: metaV1.TypeMeta{
			APIVersion: "v1",
			Kind:       "Namespace",
		},
	}
	_, err = client.Namespaces().Create(context.TODO(), &namespace, metaV1.CreateOptions{})

	if err == nil {
		return true, nil
	} else {
		return false, err
	}
}
