package config

import (
	"context"

	"encoding/base64"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func Get_Secret_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var secrets *v1.SecretList
	if namespace == "all" || namespace == "" {
		secrets, _ = client.Secrets("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		secrets, _ = client.Secrets(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, secret := range secrets.Items {
		meta := secret.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		// labels := meta.Labels
		secret_type := secret.Type

		detail := map[string]interface{}{
			"name":        name,
			"namespace":   namespace,
			"type":        secret_type,
			"create_time": create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}

func Get_Secret_Detail(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()

	secret, err := client.Secrets(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "找不到资源相关信息"})
	} else {
		meta := secret.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		labels := meta.Labels
		data := secret.Data
		data_list := make([]map[string]string, 0)
		for k, v := range data {
			var value string
			temp, err := base64.StdEncoding.DecodeString(string(v))
			if err == nil {
				value = string(temp)
			} else {
				value = string(v)
			}
			item := map[string]string{
				"key":   k,
				"value": value,
			}
			data_list = append(data_list, item)
		}
		detail := map[string]interface{}{
			"name":        name,
			"namespace":   namespace,
			"labels":      labels,
			"create_time": create_time,
			"data":        data_list,
		}
		c.JSON(200, detail)
	}
}

func Delete_Secret(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
	} else {
		client := clientset.CoreV1()
		if namespace == "all" || namespace == "" {
			c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		} else {
			err := client.Secrets(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
			if err == nil {
				c.JSON(200, gin.H{"ok": "删除成功"})
			} else {
				c.JSON(500, gin.H{"fail": "删除失败"})
			}
		}
	}

}
