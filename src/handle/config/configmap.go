package config

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_ConfigMap_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var configmaps *v1.ConfigMapList
	if namespace == "all" || namespace == "" {
		configmaps, _ = client.ConfigMaps("").List(context.TODO(), metaV1.ListOptions{})
	} else {
		configmaps, _ = client.ConfigMaps(namespace).List(context.TODO(), metaV1.ListOptions{})
	}
	list := make([]map[string]interface{}, 0)
	for _, configmap := range configmaps.Items {
		meta := configmap.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		labels := meta.Labels

		detail := map[string]interface{}{
			"name":        name,
			"namespace":   namespace,
			"labels":      labels,
			"create_time": create_time,
		}
		list = append(list, detail)
	}
	c.JSON(200, list)
}


func Get_ConfigMap_Detail(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()

	configmap, err := client.ConfigMaps(namespace).Get(context.TODO(), name, metaV1.GetOptions{})
	if err != nil {
		c.JSON(500, gin.H{"fail": "找不到资源相关信息"})
	} else {
		meta := configmap.ObjectMeta
		name := meta.Name
		namespace := meta.Namespace
		create_time := meta.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
		labels := meta.Labels
		data := configmap.Data

		detail := map[string]interface{}{
			"name":        name,
			"namespace":   namespace,
			"labels":      labels,
			"create_time": create_time,
			"data":        data,
		}
		c.JSON(200, detail)
	}
}



func Delete_ConfigMap(c *gin.Context) {
	var data InputData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	namespace := data.Namespace
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	if namespace == "all" || namespace == "" {
		c.JSON(500, gin.H{"fail": "namespace不能为空，并且不能选择all"})
		return
	}
	err = client.ConfigMaps(namespace).Delete(context.TODO(), name, metaV1.DeleteOptions{})
	if err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败"})
	}
}
