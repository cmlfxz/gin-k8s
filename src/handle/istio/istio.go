package istio

import (
	"context"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"github.com/gin-gonic/gin"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
)

type ListData struct {
	Namespace string `json:"namespace"`
}

func Get_Gateway_List(c *gin.Context) {
	var data ListData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	namespace := data.Namespace
	client, err := dynamic.NewForConfig(k8sConfig.K8sConfig)
	if err == nil {
		resource := schema.GroupVersionResource{
			Group:    "networking.istio.io",
			Version:  "v1alpha3",
			Resource: "gateways",
		}
		var result *unstructured.UnstructuredList
		result, err := client.Resource(resource).Namespace(namespace).List(context.TODO(), metaV1.ListOptions{})
		if err == nil {
			list := make([]map[string]interface{}, 0)
			for _, item := range result.Items {
				gateway := item.Object
				meta := gateway["metadata"].(map[string]interface{})
				name := meta["name"]
				spec := gateway["spec"].(map[string]interface{})
				namespace := meta["namespace"]
				selector := spec["selector"]
				servers := spec["servers"]
				// domain_list := make([]string, 0)
				// for server := range servers {
				// 	domain := server["hosts"]
				// 	domain_list = append(domain_list, domain)
				// }
				detail := map[string]interface{}{
					"name":      name,
					"namespace": namespace,
					"selector":  selector,
					"servers":   servers,
					// "domain_list": domain_list,
					// "create_time": create_time,
				}
				list = append(list, detail)
			}
			// c.JSON(200, result.Items[0])
			// c.JSON(200, list)
			c.JSON(200, gin.H{"ok": "123"})
		} else {
			c.String(500, err.Error())
		}
	} else {
		c.JSON(500, gin.H{"fail": "k8s集群链接失败"})
	}

}
