package metric

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"time"
	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"
	"gitee.com/cmlfxz/gin-k8s/src/util"
	"k8s.io/client-go/kubernetes"
)

type NodeMetrics struct {
	Kind       string `json:"kind"`
	APIVersion string `json:"apiVersion"`
	Metadata   struct {
		SelfLink          string    `json:"selfLink"`
		CreationTimestamp time.Time `json:"creationTimestamp"`
	} `json:"metadata"`
	Timestamp time.Time `json:"timestamp"`
	Window    string    `json:"window"`
	Usage     struct {
		CPU    string `json:"cpu"`
		Memory string `json:"memory"`
	} `json:"usage"`
}

func getNodeMetrics(clientset *kubernetes.Clientset, name string, metrics *NodeMetrics) error {
	path := fmt.Sprintf("apis/metrics.k8s.io/v1beta1/nodes/%s", name)
	data, err := clientset.RESTClient().Get().AbsPath(path).DoRaw(context.TODO())
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &metrics)
	return err
}

type NodeUsage struct {
	Cpu    float64 `json:"cpu"`
	Memory float64 `json:"memory"`
}

//crd 接口  kubectl get --raw /apis/metrics.k8s.io/v1beta1/pods
func Node_Usage(name string) (NodeUsage, error) {
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	var metrics NodeMetrics
	err = getNodeMetrics(clientset, name, &metrics)
	if err != nil {
		return NodeUsage{}, errors.New("获取metric失败")
	}

	node_c := util.Parse_Cpu(metrics.Usage.CPU)
	node_m := util.Parse_Memory(metrics.Usage.Memory)

	r := NodeUsage{
		Cpu:    node_c,
		Memory: node_m,
	}
	return r, nil
}


type PodMetrics struct {
	Kind       string `json:"kind"`
	APIVersion string `json:"apiVersion"`
	Metadata   struct {
		Name              string    `json:"name"`
		Namespace         string    `json:"namespace"`
		SelfLink          string    `json:"selfLink"`
		CreationTimestamp time.Time `json:"creationTimestamp"`
	} `json:"metadata"`
	Timestamp  time.Time `json:"timestamp"`
	Window     string    `json:"window"`
	Containers []struct {
		Name  string `json:"name"`
		Usage struct {
			CPU    string `json:"cpu"`
			Memory string `json:"memory"`
		} `json:"usage"`
	} `json:"containers"`
}


func getNamespacePodMetrics(clientset *kubernetes.Clientset, namespace string, name string, pods *PodMetrics) error {
	path := fmt.Sprintf("apis/metrics.k8s.io/v1beta1/namespaces/%s/pods/%s", namespace, name)
	data, err := clientset.RESTClient().Get().AbsPath(path).DoRaw(context.TODO())
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, &pods)
	return err
}

//crd 接口  kubectl get --raw /apis/metrics.k8s.io/v1beta1/pods
func Get_Pod_Usage(namespace string, name string) (map[string]float64, error) {

	// var kubeconfig *string
	// curPath, _ := os.Getwd()
	// kubeconfig = flag.String("k8s", filepath.Join(curPath, "k8s"), "(optional) relative path to the kubeconfig file")
	// flag.Parse()
	// config, _ := clientcmd.BuildConfigFromFlags("", *kubeconfig)
	// clientset, _ := kubernetes.NewForConfig(config)

	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	var metrics PodMetrics
	err = getNamespacePodMetrics(clientset, namespace, name, &metrics)
	if err != nil {
		return nil, errors.New("获取metric失败")
	}
	var pod_cpu, pod_memory float64 = 0, 0
	for _, c := range metrics.Containers {
		c_cpu := util.Parse_Cpu(c.Usage.CPU)
		c_memory := util.Parse_Memory(c.Usage.Memory)
		pod_cpu += c_cpu
		pod_memory += c_memory
	}
	pod_usage := map[string]float64{
		"cpu":    pod_cpu,
		"memory": pod_memory,
	}
	return pod_usage, nil
}
