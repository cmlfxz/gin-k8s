package pv

import (
	"context"
	"fmt"

	"gitee.com/cmlfxz/gin-k8s/src/middleware/k8sConfig"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	v1 "k8s.io/api/core/v1"
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

func pv_detail(pv *v1.PersistentVolume) map[string]interface{} {
	meta := pv.ObjectMeta
	name := meta.Name
	create_time := pv.CreationTimestamp.Local().Format("2006-01-02 15:04:05")
	spec := pv.Spec
	access_modes := spec.AccessModes[0]
	capacity := spec.Capacity["storage"]

	// var source interface{}
	ss := map[string]interface{}{
		"nfs":       spec.NFS,
		"glusterfs": spec.Glusterfs,
		"rbd":       spec.RBD,
		"hostpath":  spec.HostPath,
		"cephfs":    spec.CephFS,
	}

	var s string
	if spec.NFS != nil {
		s = "nfs"
	} else if spec.RBD != nil {
		s = "rbd"
	} else if spec.Glusterfs != nil {
		s = "glusterfs"
	} else if spec.HostPath != nil {
		s = "hostpath"
	} else if spec.CephFS != nil {
		s = "cephfs"
	}
	source := ss[s]
	pv_reclaim_policy := spec.PersistentVolumeReclaimPolicy
	storage_class_name := spec.StorageClassName
	volume_mode := spec.VolumeMode
	claim_ref := spec.ClaimRef
	var claim_namespace, claim_name, claim string = "", "", ""
	if claim_ref != nil {
		claim_namespace = claim_ref.Namespace
		claim_name = claim_ref.Name
		claim = fmt.Sprintf("%s/%s", claim_namespace, claim_name)
	}

	status := pv.Status.Phase
	detail := map[string]interface{}{
		"name":               name,
		"claim":              claim,
		"status":             status,
		"storage_class_name": storage_class_name,
		"source":             source,
		"capacity":           capacity,
		"access_modes":       access_modes,
		"pv_reclaim_policy":  pv_reclaim_policy,
		"volume_mode":        volume_mode,
		"create_time":        create_time,
	}
	return detail
}

func Get_PV_List(c *gin.Context) {
	var err error
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	var pvs *v1.PersistentVolumeList

	pvs, _ = client.PersistentVolumes().List(context.TODO(), metaV1.ListOptions{})
	list := make([]map[string]interface{}, 0)
	for _, pv := range pvs.Items {
		detail := pv_detail(&pv)
		list = append(list, detail)
	}
	c.JSON(200, list)
}

type SelectData struct {
	Name string `json:"name"`
}

func Select_PV(c *gin.Context) {
	var data SelectData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s链接失败"})
		return
	}
	client := clientset.CoreV1()
	// var*v1.PersistentVolume, error)
	pv, err := client.PersistentVolumes().Get(context.TODO(), name, metaV1.GetOptions{})
	if err == nil {
		detail := pv_detail(pv)
		c.JSON(200, detail)
	} else {
		c.JSON(500, gin.H{"fail": "查询不到pv的数据"})
	}
}

func Create_PV(c *gin.Context) {
	var s Storage
	if err := c.ShouldBindBodyWith(&s, binding.JSON); err != nil {
		c.JSON(500, gin.H{"fail": "存储参数错误"})
	} else {

		var pv *v1.PersistentVolume
		if s.Source == "nfs" {
			var nfss NFSStorage
			if err := c.ShouldBindBodyWith(&nfss, binding.JSON); err != nil {
				c.JSON(500, gin.H{"fail": "存储参数错误"})
			} else {
				pv = nfss.Create(s.Name)
			}
		} else {
			c.JSON(500, gin.H{"fail": "目前只支持nfs pv"})
		}
		clientset, _ := kubernetes.NewForConfig(k8sConfig.K8sConfig)
		client := clientset.CoreV1()
		_, err := client.PersistentVolumes().Create(context.TODO(), pv, metaV1.CreateOptions{})
		if err == nil {
			c.JSON(200, gin.H{"ok": "创建成功"})
		} else {
			fmt.Println(err)
			c.JSON(500, gin.H{"fail": "创建失败", "error": err.Error()})
		}
	}
}

type DeleteData struct {
	Name string `json:"name"`
}

func Delete_PV(c *gin.Context) {
	var data DeleteData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	name := data.Name

	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)

	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()

	err = client.PersistentVolumes().Delete(context.TODO(), name, metaV1.DeleteOptions{})
	if err == nil {
		c.JSON(200, gin.H{"ok": "删除成功"})
	} else {
		c.JSON(500, gin.H{"fail": "删除失败"})
	}

}

type MultiDeleteData struct {
	PV_List []string `json:"pv_list"`
}

func Delete_Multi_PV(c *gin.Context) {
	var data MultiDeleteData
	if err := c.ShouldBindJSON(&data); err != nil {
		c.JSON(500, gin.H{"fail": "传参错误"})
		return
	}
	clientset, err := kubernetes.NewForConfig(k8sConfig.K8sConfig)
	if err != nil {
		c.JSON(500, gin.H{"fail": "k8s连接不上"})
		return
	}
	client := clientset.CoreV1()
	var success_list []string
	var fail_list []string
	for _, name := range data.PV_List {
		err := client.PersistentVolumes().Delete(context.TODO(), name, metaV1.DeleteOptions{})
		if err == nil {
			fmt.Println(err)
			success_list = append(success_list, name)
		} else {
			fail_list = append(fail_list, name)
		}
	}
	c.JSON(200, gin.H{"success_list": success_list, "fail_list": fail_list})

}
