package model

import (

	// "gitee.com/cmlfxz/gin-k8s/conf"

	"gorm.io/gorm"
)

var (
	Db *gorm.DB
)

// func Setup() {
// 	var err error
// 	mysqlConf := conf.Setting.MySQLConfig
// 	host := mysqlConf.Host
// 	port := mysqlConf.Port
// 	database := mysqlConf.DataBase
// 	username := mysqlConf.UserName
// 	password := mysqlConf.PassWord

// 	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", host, port, database, username, password)
// 	// dsn := "dev_user:abc123456@tcp(192.168.11.200:52100)/tutorial?charset=utf8mb4&parseTime=True&loc=Local"
// 	Db, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
// 	if err != nil {
// 		fmt.Println("数据库连接失败")
// 		panic(err)
// 	}
// 	// defer db.Close()
// 	fmt.Println("数据库连接成功", &Db)
// }
