package conf

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"

	"gopkg.in/yaml.v3"
)

type Config struct {
	RedisConfig *RedisConfig `yaml:"redis"`
	MySQLConfig *MySQLConfig `yaml:"mysql"`
}

var Setting *Config

type RedisConfig struct {
	Addr     string
	Password string
}

type MySQLConfig struct {
	Host     string
	Port     int
	DataBase string
	UserName string
	PassWord string
}

func LoadConfig() {
	filename := "dev.yml"
	env := os.Getenv("ENV")
	if env == "test" {
		filename = "test.yml"
	} else if env == "prod" {
		filename = "prod.yml"
	}
	// pwd, _ := os.Getwd()
	fullPath := path.Join("conf", filename)

	f, err := os.Open(fullPath)
	if nil != err {
		fmt.Println(err)
		panic(err)
	}
	defer f.Close()

	buf, _ := ioutil.ReadAll(f)

	config := new(Config)
	err = yaml.Unmarshal(buf, config)
	if nil != err {
		fmt.Println(err)
		panic(err)
	}
	Setting = config
}
